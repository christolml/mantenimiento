package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
)

func main() {

	files, err := ioutil.ReadDir("Archivos")
	if err != nil {
		log.Fatal(err)
	}
	contHTML, contAplicacion, contIP, contCorreo, contSQL, contConexBD, contOculto := 0, 0, 0, 0, 0, 0, 0

	for _, file := range files {

		archivo := file.Name()
		content, err := ioutil.ReadFile("Archivos/" + archivo)
		if err != nil {
			log.Fatal(err)
		}
		texto := string(content)

		html := regexp.MustCompile(`\<!--`)
		aplicacion := regexp.MustCompile(`\/\*|\/\/`)
		ip := regexp.MustCompile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}`)
		correo := regexp.MustCompile(`\.com`)
		sql := regexp.MustCompile(`\SELECT`)
		db := regexp.MustCompile(`\Database=`)
		oculto := regexp.MustCompile(`hidden`)

		fmt.Println(file.Name())
		subHTML := html.FindAllString(texto, -1)
		for i := 1; i <= len(subHTML); i++ {
			contHTML++
		}
		subAplicacion := aplicacion.FindAllString(texto, -1)
		for i := 1; i <= len(subAplicacion); i++ {
			contAplicacion++
		}
		subIP := ip.FindAllString(texto, -1)
		for i := 1; i <= len(subIP); i++ {
			contIP++
		}
		subCorreo := correo.FindAllString(texto, -1)
		for i := 1; i <= len(subCorreo); i++ {
			contCorreo++
		}
		subSQL := sql.FindAllString(texto, -1)
		for i := 1; i <= len(subSQL); i++ {
			contSQL++
		}
		subDB := db.FindAllString(texto, -1)
		for i := 1; i <= len(subDB); i++ {
			contConexBD++
		}
		subOculto := oculto.FindAllString(texto, -1)
		for i := 1; i <= len(subOculto); i++ {
			contOculto++
		}

		fmt.Println("Comentarios HTML: ", contHTML)
		fmt.Println("Comentarios de la aplicación: ", contAplicacion)
		fmt.Println("Direcciones IP: ", contIP)
		fmt.Println("Direcciones de correo electrónico: ", contCorreo)
		fmt.Println("Consultas SQL: ", contSQL)
		fmt.Println("Cadenas de conexión a la base de datos: ", contConexBD)
		fmt.Print("Campos ocultos de entrada: ", contOculto, "\n")

	}

	fmt.Print("\nResumen\n",
		"Comentarios HTML:", contHTML, "\n",
		"Comentarios de la aplicación: ", contAplicacion, "\n",
		"Direcciones IP: ", contIP, "\n",
		"Direcciones de correo electrónico: ", contCorreo, "\n",
		"Consultas SQL: ", contSQL, "\n",
		"Cadenas de conexión a la base de datos: ", contConexBD, "\n",
		"Campos ocultos de entrada: ", contOculto)

}
