package main

import (
	"github.com/christolml/mantenimiento/automatizar-pruebas-unitarias-2018-Christolml-master/pruebasUnitarias" // jumper
	// "testeo-GO/pruebasUnitarias"  toshiba
	"fmt"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Error: ", r)
		}
	}()

	datos := []int{35, 32, 34, 34, 32, 36, 30, 37, 30, 39}

	//media aritmética
	fmt.Println(" Media Aritmética\n", pruebasUnitarias.MediaAritmetica(datos))

	//media geométrica
	// fmt.Println(pruebasUnitarias.MediaGeometrica([]int{22, 25, 19, 18, 18, 18, 22, 21, 22, 19}))
	fmt.Println(" Media Geométrica:\n", pruebasUnitarias.MediaGeometrica(datos))

	//media armónica
	// fmt.Println(" Media Armónica:\n", pruebasUnitarias.MediaArmonica(datos))

}
