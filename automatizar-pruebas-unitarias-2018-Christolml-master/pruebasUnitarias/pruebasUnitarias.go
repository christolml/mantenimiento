package pruebasUnitarias

import (
	"errors"
	"math"
)

func pruebasUnitarias() {
	// fmt.Println(raizEnesima(25, 2))
}

//Calcula y regresa la media aritmética
func MediaAritmetica(vals []int) float64 {
	var suma int
	for _, array := range vals {
		suma += array
	}

	// resultado := math.Trunc(float64(suma)/float64(len(vals))*100) / 100 //2 decimales
	resultado := math.Trunc((float64(suma)/float64(len(vals)))*10000) / 10000 //4 decimales
	// return float64(suma) / float64(len(vals))	resultado original
	return resultado
}

//Calcula y regresa la raíz enésima
func raizEnesima(base, exponente float64) float64 {

	if base < 0 || exponente < 0 {
		panic("Los números deben ser positivos")
	} else {
		resultado := math.Pow(base, (1 / exponente))

		if (math.Round(resultado)-resultado) < 0.1 && (math.Round(resultado)-resultado) > 0 {
			//me redondea el resultado
			return math.Round(resultado)
		} else {
			//este sirve para quitar decimales a mi resultado
			return float64(int(resultado*100)) / 100
		}
	}
}

//Usa raizEnesima para calcular y regresar le media geométrica
func MediaGeometrica(vals []int) float64 {
	mult := 1
	for _, array := range vals {
		mult *= array
	}
	return raizEnesima(float64(mult), float64(len(vals)))
}

// metodo no implementado
func MediaArmonica(vals []int) (float64, error) {
	err := errors.New("Método no implementado")

	return 0, err
}

//Calcula y regresa la media armónica
// func MediaArmonica(vals []int) float64 {
// 	var result float32
// 	for _, array := range vals {
// 		result += (1 / float32(array))
// 	}
// 	resultado := math.Trunc((float64(len(vals))/float64(result))*10000) / 10000 //4 decimales
// 	return resultado
// }
