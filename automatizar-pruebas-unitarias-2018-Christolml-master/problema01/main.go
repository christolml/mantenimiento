package main

import (
	"github.com/christolml/mantenimiento/automatizar-pruebas-unitarias-2018-Christolml-master/pruebasUnitarias"
	"fmt"
	"strconv"
	"time"

	"bufio"
	"os"
	"strings"
)

func main() {

	sgInicio := time.Now().Local().Format("05")
	msInicio := time.Now().Local().Format(".000")

	/* Variables globales */
	var campos []string
	var operacionExitosa int
	var operacionFallida int

	archivo, err := os.Open("./datos.txt") //se abre el archivo

	if err != nil {
		panic("Hubo un error al abrir el archivo")
	}

	//le pasamos el archivo con que vamos a trabajar a nuestro scanener
	scanner := bufio.NewScanner(archivo)

	/* se lee linea por linea y se van guardando los campos en un array
	for scanner	-> se itera las lineas del archivo, con Scan se va leyendo las lineas
	parametros := strings	-> se guarda en el array por cada iteracion los datos de cada linea del texto datos.txt
	campos = append	-> se guardan los campos en un array global
	*/
	for scanner.Scan() {
		parametros := strings.Split(scanner.Text(), ":")
		for _, parametro := range parametros {
			campos = append(campos, parametro)
		}
	}
	// for i, campo := range campos {			imprime los campos
	// 	fmt.Println(i, campo)
	// }

	/* 	"vueltas" nos permite saber cuantas lineas hay en el txt, para poder usarla como
	referencia a la conversion de los dato de entrada y salida a convertirlos a enteros
	inicioEntrada	-> representa el indice de donde empiezan los datos de entrada
	inicioSalida	-> representa el indice de donde empiezan los datos de salida
	*/
	vueltas := len(campos) / 4
	inicioMetodo := 1
	inicioEntrada := 2
	inicioSalida := 3

	/* nos convierte los datos de entrada a un array de enteross y nos convierte los datos de salida a un array de enteros
	for b := 0; b < len(entradasString)	-> para entrada
	valor, _ := strconv.Atoi 	-> me convierte de string a int
	valor, _ := strconv.ParseFloat	-> me convierte la salidda a un float
			var salidaInt []float64
			salidaInt = append(salidaInt, valor)	este y el de arriba sirve para almecenar las salidas en un array de float
	*/
	for i := 0; i < vueltas; i++ {

		entradasString := strings.Split(campos[inicioEntrada], " ")
		var entradasInt []int

		if campos[inicioMetodo] == "mediaAritmetica" {
			if campos[inicioEntrada] != "" && campos[inicioSalida] != "" {

				for b := 0; b < len(entradasString); b++ {
					valor, err2 := strconv.Atoi(entradasString[b])
					if err2 != nil {
						entradasInt = nil
						break
					} else {
						entradasInt = append(entradasInt, valor)
					}
				}

				valorReal, err := strconv.ParseFloat(campos[inicioSalida], 64)
				resultado := pruebasUnitarias.MediaAritmetica(entradasInt)

				if entradasInt == nil && err != nil {
					fmt.Println("Hay letras en los datos de entrada y salida")
					operacionFallida++
				} else {
					if entradasInt == nil {
						fmt.Println("Hay letras en los datos de entrada")
						operacionFallida++
					} else {
						if err != nil {
							fmt.Println("El dato de salida no es un número")
							operacionFallida++
						} else {
							if valorReal == resultado {
								fmt.Println("El resultado tuvo exito, el método empleado fue", campos[inicioMetodo], "y el resultado es", resultado)
								operacionExitosa++
							} else {
								fmt.Println("El resultado fracaso, revisa los datos de entrada o salida")
								operacionFallida++
							}
						}
					}

				}

			} else {
				fmt.Println("Los datos de entrada o salida no estan completos, revisa los datos")
				operacionFallida++
			}

		} else if campos[inicioMetodo] == "mediaGeometrica" {
			fmt.Println("Estas dentro de media geometrica")

		} else if campos[inicioMetodo] == "mediaArmonica" {
			fmt.Println("Estas dentro de media armonica")

		} else {
			fmt.Println("Ingresa un método valido")
		}

		inicioEntrada += 4
		inicioSalida += 4
		inicioMetodo += 4

	}
	fmt.Println("\nResumen\nEn total hubo", operacionExitosa, "operaciones exitosas\nEn total hubo", operacionFallida, "operaciones fallidas")

	// time.Sleep(3 * time.Second)   calando los segundo NO BORRAR
	sgFinal := time.Now().Local().Format("05")
	msFinal := time.Now().Local().Format(".000")
	sgInicioInt, _ := strconv.Atoi(sgInicio)
	sgFinalInt, _ := strconv.Atoi(sgFinal)

	msInicioFloat, _ := strconv.ParseFloat(msInicio, 64)
	msFinalFloat, _ := strconv.ParseFloat(msFinal, 64)
	sgResultado := sgFinalInt - sgInicioInt
	msResultado := msFinalFloat - msInicioFloat
	fmt.Printf("\nLas operaciones se tardaron %d segundos y %.3f ms en ejecutarse", sgResultado, msResultado)

	// fmt.Println("segundos", sgFinal)
	// fmt.Println(msInicio, "\n", msFinal)
	// fmt.Printf("%.3f", msResultado)
	// currenttime := time.Now().Local()
	// fmt.Println("Current time : ", currenttime.Format("2006-01-02 15:04:05 +0800

}
