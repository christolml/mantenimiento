package main

import (
	"github.com/christolml/mantenimiento/automatizar-pruebas-unitarias-2018-Christolml-master/pruebasUnitarias"
	"fmt"
	"strconv"
	"time"

	"bufio"
	"os"
	"strings"
)

var inicioFolio = 0
var inicioMetodo = 1
var inicioEntrada = 2
var inicioSalida = 3
var operacionExitosa int
var operacionFallida int

func main() {

	sgInicio := time.Now().Local().Format("05")
	msInicio := time.Now().Local().Format(".000")

	var campos []string

	archivo, err := os.Open("./CasosPrueba.txt")

	if err != nil {
		panic("Hubo un error al abrir el archivo")
	}

	//le pasamos el archivo con que vamos a trabajar a nuestro scanener
	scanner := bufio.NewScanner(archivo)

	for scanner.Scan() {
		parametros := strings.Split(scanner.Text(), ":")
		for _, parametro := range parametros {
			campos = append(campos, parametro)
		}
	}

	vueltas := len(campos) / 4

	for i := 0; i < vueltas; i++ {

		entradasString := strings.Split(campos[inicioEntrada], " ")

		if campos[inicioMetodo] == "mediaAritmetica" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		} else if campos[inicioMetodo] == "mediaGeometrica" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		} else if campos[inicioMetodo] == "mediaArmonica" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		} else {
			fmt.Println(campos[inicioFolio], "ERROR método no valido")
		}

		inicioFolio += 4
		inicioEntrada += 4
		inicioSalida += 4
		inicioMetodo += 4

	}
	fmt.Println("\nResumen\nEn total hubo", operacionExitosa, "operaciones exitosas\nEn total hubo", operacionFallida, "operaciones fallidas")

	sgFinal := time.Now().Local().Format("05")
	msFinal := time.Now().Local().Format(".000")
	sgInicioInt, _ := strconv.Atoi(sgInicio)
	sgFinalInt, _ := strconv.Atoi(sgFinal)

	msInicioFloat, _ := strconv.ParseFloat(msInicio, 64)
	msFinalFloat, _ := strconv.ParseFloat(msFinal, 64)
	sgResultado := sgFinalInt - sgInicioInt
	msResultado := msFinalFloat - msInicioFloat
	fmt.Printf("\nLas operaciones se tardaron %d segundos y %.3f ms en ejecutarse", sgResultado, msResultado)

}

func haciendoMetodo(campos []string, entradasString []string, metodo string) {
	var entradasInt []int
	var resultado float64

	if campos[inicioEntrada] != "" && campos[inicioSalida] != "" {

		for b := 0; b < len(entradasString); b++ {
			valor, err2 := strconv.Atoi(entradasString[b])
			if err2 != nil {
				entradasInt = nil
				break
			} else {
				entradasInt = append(entradasInt, valor)
			}
		}

		valorReal, err := strconv.ParseFloat(campos[inicioSalida], 64)
		// resultado := pruebasUnitarias.MediaAritmetica(entradasInt)

		if campos[inicioEntrada] == "NULL" && valorReal == 0 {
			fmt.Println(campos[inicioFolio], "Éxito", campos[inicioMetodo], "=", resultado)
			operacionExitosa++
		} else {
			if metodo == "mediaAritmetica" {
				resultado = pruebasUnitarias.MediaAritmetica(entradasInt)
			} else if metodo == "mediaGeometrica" {
				resultado = pruebasUnitarias.MediaGeometrica(entradasInt)

			} else if metodo == "mediaArmonica" {

				resultado, err = pruebasUnitarias.MediaArmonica(entradasInt)
				if err != nil {
					fmt.Println("Error", err)
					return
					// sin el return de arriba estaría ejecutando "fmt.Println(resultado, err)" y el resultado me devuelve un 0 ya que ese es el valor por defecto de float64
				}
			}

			if entradasInt == nil && err != nil {
				fmt.Println(campos[inicioFolio], "*Falla* ") //Hay letras en los datos de entrada y salida
				operacionFallida++
			} else {
				if entradasInt == nil {
					fmt.Println(campos[inicioFolio], "Falla") //Hay letras en los datos de entrada
					operacionFallida++
				} else {
					if err != nil {
						fmt.Println(campos[inicioFolio], "Falla", campos[inicioMetodo], "= 0 esperado", campos[inicioSalida]) //El dato de salida no es un número
						operacionFallida++
					} else {
						if valorReal == resultado {
							fmt.Println(campos[inicioFolio], "Éxito", campos[inicioMetodo], "=", resultado)
							operacionExitosa++
						} else {
							fmt.Println(campos[inicioFolio], "Falla", campos[inicioMetodo], "=", resultado, "esperado=", campos[inicioSalida]) //El resultado fracaso revisa los datos de entrada o salida
							operacionFallida++
						}
					}
				}
			}
		}

	} else {
		fmt.Println(campos[inicioFolio], "Falla") //"Los datos de entrada o salida no estan completos, revisa los datos
		operacionFallida++
	}
}
