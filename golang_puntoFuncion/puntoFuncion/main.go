package main

import (
	"../pruebasPuntoFuncion"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var inicioFolio = 0
var inicioMetodo = 1
var inicioEntrada = 2

func main() {


	var campos []string

	archivo, err := os.Open("./datos.txt")

	if err != nil {
		panic("Hubo un error al abrir el archivo")
	}

	//le pasamos el archivo con que vamos a trabajar a nuestro scanener
	scanner := bufio.NewScanner(archivo)

	for scanner.Scan() {
		parametros := strings.Split(scanner.Text(), ":")
		for _, parametro := range parametros {
			campos = append(campos, parametro)
		}
	}

	vueltas := len(campos) / 3

	for i := 0; i < vueltas; i++ {

		entradasString := strings.Split(campos[inicioEntrada], " ")

		if campos[inicioMetodo] == "simple" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		} else if campos[inicioMetodo] == "promedio" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		} else if campos[inicioMetodo] == "complejo" {
			haciendoMetodo(campos, entradasString, campos[inicioMetodo])

		}

		inicioFolio += 3
		inicioEntrada += 3
		inicioMetodo += 3

	}
	
}

func haciendoMetodo(campos []string, entradasString []string, metodo string) {
	var entradasInt []int

	if campos[inicioEntrada] != "" {

		for b := 0; b < len(entradasString); b++ {
			valor, err2 := strconv.Atoi(entradasString[b])
			if err2 != nil {
				entradasInt = nil
				break
			} else {
				entradasInt = append(entradasInt, valor)
			}
		}



			if metodo == "simple" {
				fmt.Println("Conteo Final por Simple:", pruebasPuntoFuncion.Simple(entradasInt))
			} else if metodo == "promedio" {
				fmt.Println("Conteo Final por Promedio:", pruebasPuntoFuncion.Promedio(entradasInt))
				
			} else if metodo == "complejo" {
				fmt.Println("Conteo Final por Complejo:", pruebasPuntoFuncion.Complejo(entradasInt))
			}
			
		

	} else {
		fmt.Println(campos[inicioFolio], "Falla") //"Los datos de entrada o salida no estan completos, revisa los datos
	}
}
