package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var inicioRetiro = 0
var inicioSaldo = 1

func main() {

	var campos []string

	archivo, err := os.Open("./CasosPrueba.txt")

	if err != nil {
		panic("Hubo un error al abrir el archivo")
	}

	//le pasamos el archivo con que vamos a trabajar a nuestro scanener
	scanner := bufio.NewScanner(archivo)

	for scanner.Scan() {
		parametros := strings.Split(scanner.Text(), ":")
		for _, parametro := range parametros {
			campos = append(campos, parametro)
		}
	}

	vueltas := len(campos) / 2

	for i := 0; i < vueltas; i++ {

		if campos[inicioRetiro] != "" && campos[inicioSaldo] != "" {

			valorRetiro, err1 := strconv.ParseFloat(campos[inicioRetiro], 64)
			valorSaldo, err2 := strconv.ParseFloat(campos[inicioSaldo], 64)

			saldoFinal, err := Retirar(valorRetiro, valorSaldo, err1, err2)
			if err != nil {
				fmt.Println("Error", err)

			} else {
				fmt.Println("Exito saldo final ", saldoFinal)
			}

		} else {
			fmt.Println("Los datos de entrada o salida no estan completos, revisa los datos")
		}

		inicioRetiro += 2
		inicioSaldo += 2

	}
	// fmt.Println("\nResumen\nEn total hubo", operacionExitosa, "operaciones exitosas\nEn total hubo", operacionFallida, "operaciones fallidas")

}

func Retirar(retirar, saldo float64, error1 error, error2 error) (float64, error) {
	var err error
	if error1 != nil || error2 != nil {
		err = errors.New("FALLA")
	} else {
		if retirar <= 0 {
			err = errors.New("FALLA")
		} else if retirar > saldo {
			err = errors.New("FALLA")
		} else {
			saldo -= retirar
		}
	}

	return saldo, err

}
